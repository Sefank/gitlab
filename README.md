# GitLab

## 测试覆盖率

- [![Ruby 覆盖率](https://gitlab.com/gitlab-org/gitlab-ce/badges/master/coverage.svg?job=coverage)](https://gitlab-org.gitlab.io/gitlab-ce/coverage-ruby) Ruby
- [![JavaScript 覆盖率](https://gitlab.com/gitlab-org/gitlab-ce/badges/master/coverage.svg?job=karma)](https://gitlab-org.gitlab.io/gitlab-ce/coverage-javascript) JavaScript

## 官方源代码

GitLab 社区版(Community Edition) 的官方源代码托管于 [GitLab.com](https://gitlab.com/gitlab-org/gitlab-ce/).

## 用于代码协作的开放源码软件

要了解 GitLab 的外观，请参阅[我们网站上的功能页面](https://about.gitlab.com/features/)。

- 使用精细的访问控制来管理 Git 存储库，从而确保您的代码安全
- 执行代码审查并增强与合并请求的协作
- 完整的 持续集成(CI) 和 CD 管道，以构建、测试和部署您的应用程序
- 每个项目还可设问题跟踪器、问题讨论区和维基
- GitLab 是最受欢迎的内部管理Git存储库的解决方案，超过 10 万个组织在用它
- 完全自由且开源（采用 MIT Expat 许可证）

## 招聘

我们一直在招聘开发、支持和生产工程师，详情见我们的[岗位页（英文）](https://about.gitlab.com/jobs/)。

## 版本

共有两个版本的GitLab：

- GitLab 社区版(Community Edition, CE) 可在 MIT Expat 许可下免费获得。
- GitLab 企业版(Enterprise Edition, EE) 包括对于拥有超过100个用户的组织更有用的[附加功能](https://about.gitlab.com/pricing/#compare-options)。要使用企业版并获得官方支持，请[订阅](https://about.gitlab.com/pricing/)。

## 网站

在 [about.gitlab.com](https://about.gitlab.com/) 上，您可以找到有关以下内容的更多信息： 

- [订阅价格](https://about.gitlab.com/pricing/)
- [咨询](https://about.gitlab.com/consultancy/)
- [社区](https://about.gitlab.com/community/)
- 使用 [托管于 GitLab.com](https://about.gitlab.com/gitlab-com/) 的免费 GitLab 服务
- [GitLab 企业版](https://about.gitlab.com/features/#enterprise) 具有的针对大型组织的附加功能
- [GitLab CI](https://about.gitlab.com/gitlab-ci/) 易于与 GitLab 整合的 持续集成(CI) 服务器。

## 系统要求

有关系统要求的信息，请参阅[要求文档](doc/install/requirements.md)以及有关支持的操作系统的更多信息。

## 安装

安装 GitLab 的推荐方法是使用我们的软件包服务器上的 [Omnibus 软件包](https://about.gitlab.com/downloads/)。
与源代码安装相比，速度更快，也更不容易出错。
只需选择您的操作系统，下载相应的软件包（Debian 或 RPM）并使用系统的软件包管理器进行安装。

安装 GitLab 还有其他各种选项，请参阅 [GitLab 网站上的安装说明页](https://about.gitlab.com/installation/) 以获取更多信息。

## 贡献

GitLab 是一个开源项目，我们非常乐意接受社区贡献。有关详细信息，请参阅 [为 GitLab 贡献](https://about.gitlab.com/contributing/) 页。

## 许可

GitLab 社区版(Community Edition, CE) 可在 MIT Expat 许可下免费获得。

GitLab 软件中包含的所有第三方组件均根据适用组件所有者提供的原始许可进行许可。

保存在此仓库中`doc/`目录下的所有文档内容均在 Creative Commons：CC BY-SA 4.0 下获得许可。

## 安装开发环境

要使用 GitLab 本身开发，我们建议您使用 [GitLab 开发工具包](https://gitlab.com/gitlab-org/gitlab-development-kit) 来设置开发环境。
如果您不使用 GitLab 开发工具包，您需要自己安装和设置所有依赖项，这不仅很麻烦且很容易出错。
自己安装它时还需要做的一件小事就是复制示例开发 unicorn 配置文件：

    cp config/unicorn.rb.example.development config/unicorn.rb

有关如何启动 GitLab 以及如何运行测试的说明，请参阅 [GitLab 开发工具包的入门部分](https://gitlab.com/gitlab-org/gitlab-development-kit#getting-started)。

## 软件依赖链

GitLab 是 Ruby on Rails 应用程序，且需要以下软件才能运行： 

- Ubuntu/Debian/CentOS/RHEL/OpenSUSE
- Ruby (MRI) 2.4
- Git 2.8.4+
- Redis 2.8+
- PostgreSQL（推荐） or MySQL

有关更多信息，请参阅[构架文档](https://docs.gitlab.com/ce/development/architecture.html)。

## 用户体验(UX) 设计

在创建设计和实现代码时，请遵守 [UX 指南](doc/development/ux_guide/index.md)。

## 第三方应用程序

有很多[第三方应用程序与 GitLab 集成](https://about.gitlab.com/applications/)，包括 GUI Git 客户端、移动应用程序和各种语言的 API 包装器。

## GitLab 发布周期

有关发布过程的更多信息，请参阅[发布文档](https://gitlab.com/gitlab-org/release-tools/blob/master/README.md)。

## 升级

有关升级信息，请参阅我们的[更新页面](https://about.gitlab.com/update/)。

## 文档

所有文档都可以在 [docs.gitlab.com/ce/](https://docs.gitlab.com/ce/) 上找到。

## 获得帮助

有关获取帮助的众多选项，请参阅我们网站中的[获取 GitLab 的帮助](https://about.gitlab.com/getting-help/)页面。

## 为什么使用 GitLab ？

请阅读[这篇文章](https://about.gitlab.com/why/)。

## 使用 GitLab 有好处吗？

[当然有](https://news.ycombinator.com/item?id=3067434)！

## GitLab 好用吗？

[这些人](https://twitter.com/gitlab/likes)似乎比较喜欢 GitLab。
